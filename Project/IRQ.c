/******************************************************************************/
/* IRQ.C: IRQ Handler                                                         */
/******************************************************************************/
/* This file is part of the uVision/ARM development tools.                    */
/* Copyright (c) 2005-2006 Keil Software. All rights reserved.                */
/* This software may only be used under the terms of a valid, current,        */
/* end user licence from KEIL for a compatible version of KEIL software       */
/* development tools. Nothing else gives you the right to use this software.  */
/******************************************************************************/

#include <91x_lib.h>
#include <RTL.h>
#include "Dashboard.h"


short IRSensor, Potentiometer, SlideSensor;
extern OS_MBX mailbox1;				/* Mailbox decleration */
extern U32 mpool[20*(2*sizeof(struct adc_mbox))/4 + 3]; /* Declare memory pool */

unsigned char AD_in_progress;           /* AD conversion in progress flag     */

__irq void ADC_IRQ_Handler (void) {     /* AD converter interrupt routine     */

	struct adc_mbox *adc_buffer = NULL;
	
  adc_buffer = (struct adc_mbox *) _alloc_box(mpool);
	
	Potentiometer = ADC->DR0 & 0x03FF;    /* AD value for global usage (10 bit) */		
	SlideSensor = ADC->DR1 & 0x03FF;      /* AD value for global usage (10 bit) */	//dr4.1
	IRSensor = ADC->DR2 & 0x03FF;					
	
	adc_buffer->Potentiometer = Potentiometer;
	adc_buffer->SliderSensor = SlideSensor;
	adc_buffer->IRSensor = IRSensor;
	
	os_mbx_send (&mailbox1, adc_buffer, 0xFFFF);
	
	ADC->CR &= 0xFFFE;                    /* Clear STR bit (Start Conversion)   */
  ADC->CR &= 0x7FFF;                    /* Clear End of Conversion flag       */

  VIC0->VAR = 0;                        /* Acknowledge Interrupt              */  
  VIC1->VAR = 0;

  AD_in_progress = 0;                   /* Clear flag, as AD conv finished    */
}
