/* Dashboard header file */

#define MAX_LCD_CHAR	16
#define MAX_SPEED			200
#define LED_ON				1
#define LED_OFF				0
#define LED_0					0x01
#define LED_1					0x02
#define LED_2					0x04
#define LED_3					0x08
#define LED_4					0x10
#define LED_5					0x20
#define LED_6					0x40
#define LED_7					0x80
#define FIRST_LINE		" WARN SLOW DOWN "

struct adc_mbox {
	unsigned short Potentiometer;
	unsigned short SliderSensor;
	unsigned short IRSensor;
};
