/*----------------------------------------------------------------------------
 *      RL-ARM - RTX
 *----------------------------------------------------------------------------
 *      Name:    DASHBOARD_TEMPLATE.C
 *      Purpose: RTX example program
 *----------------------------------------------------------------------------
 *      This code has been modified from the example Blinky project of the
				RealView Run-Time Library.
 *      Copyright (c) 2004-2011 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------
 *			Contributor to Dashboard.c
 *			A0134665B
 *			A0136599L
 *			A0026294E
 *---------------------------------------------------------------------------*/

#include <RTL.h>
#include <91x_lib.H>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LCD.h"
#include "Dashboard.h"

extern __irq void ADC_IRQ_Handler (void); /* ADC  interrupt routine           */
extern unsigned char AD_in_progress;      /* AD conversion in progress flag  */
unsigned int counter;
unsigned int ambient_light;
unsigned char SENSOR = 0;
enum PWM_States {PWM_LED_OFF, PWM_LED_ON } PWM_State;
enum INT_LIGHT_States { INT_LIGHT_OFF, INT_LIGHT_ON, INT_LIGHT_DIM } INT_LIGHT_State;
enum ALARM_States { ALARM_OFF, ALARM_ON } ALARM_State;
enum ENGINE_States { ENGINE_OFF, ENGINE_ON } ENGINE_State;
enum DOOR_States { DOOR_CLOSE, DOOR_OPEN } DOOR_State;

unsigned char A0,A1; //A0 - Button 3.5, A1 - Button 3.6
unsigned char B0 = 0,B1 = 0,B2 = 0,B3 = 0,B4 = 0,B5 = 0,B6 = 0,B7 = 0; //B0-B7 represent LED's 0 through 7

// variables for engine and door
unsigned char engine_release_count=0, door_release_count=0;
//unsigned char doorStatus_transit=0;
unsigned char engineStatus, doorStatus, collisionAvoidance;
int ambient_threshold = 3; 
int doorOpen_previously = 0;

//variables for pwm 
unsigned int head_light_status;
unsigned int high_value  = 0;
unsigned int low_value = 5;
unsigned int high_value_internal_light = 0;
unsigned int low_value_internal_light = 5;
unsigned int light_on_when_dimming = 0;
//global variable to turn off head_light according to engine status;
unsigned int headlight_off = 0;


/* Declare mutex for LCD & LED */
OS_MUT LED_mutex, LCD_mutex;
/* declare mailbox for 20 msgs */
os_mbx_declare(mailbox1, 20);		
/* memory pool with 20 entries */
U32 mpool[20*(2*sizeof(struct adc_mbox))/4 + 3];

//Function to read input
void read_buttons()
{
	//BUTTON_3_5:
	A0 = !(GPIO3->DR[0x080]>>5); // Returns 1 when pressed and 0 when released
	//BUTTON_3_6:
	A1 = !(GPIO3->DR[0x100]>>6); // Returns 1 when pressed and 0 when released

}

/* @ Function to write to the LED GPIO ports
 * @ led : which LED bits to set to ON/OFF 
 * @ state : LED_ON / LED_OFF
 */
void write_led(U8 led, U8 LED_on)
{
  unsigned char mask;
	int i;
	
	os_mut_wait(&LED_mutex, 0xffff);	/* CS start, wait for mutex */
	mask = GPIO7->DR[0x3FC];
	
	/* GPIO pin toggle to 1 if state = ON */
	/* GPIO pin toggle to 0 if state = OFF */
	if (LED_on) {
		for (i = 0; i < 8; i++)
			mask |= (led & (1 << i));
	} else {
		for (i = 0; i < 8; i++)
			mask &= ~(led & (1 << i));
	}
	
  GPIO7->DR[0x3FC] = mask;
	os_mut_release(&LED_mutex);		/* release mutex after CS */
	
}

 void head_light_on()
 {
	head_light_status = 1;
	write_led(0x07,LED_ON);
 }
 
 void head_light_off()
 {
	head_light_status = 0;
	write_led(0x07,LED_OFF);
 }
 
 void internal_light_on()
 {
	 write_led(0x38,LED_ON);
 }
 
 void internal_light_off()
 {
	 write_led(0x38,LED_OFF);
 }

 void alarm_on()
 {
	 write_led(0xC0,LED_ON);
 }
 
 void alarm_off()
 {
	 write_led(0xC0,LED_OFF);
 }

void start_ADC ( )
{
	if (!AD_in_progress)  
	{	/* If conversion not in progress      */
        AD_in_progress = 1;                 /* Flag that AD conversion is started */
        ADC->CR |= 0x0423;                  /* Set STR bit (Start Conversion)     */
  }
	//Now the interrupt will be called when conversion ends
}


OS_TID IO_output_id, ADC_task_id; // Declare variable t_Lighting to store the task id
OS_TID MAIN_state_machine;

/*----------------------------------------------------------------------------
 *        Task 1 'Task_ADC_Con': ADC Conversion
 *---------------------------------------------------------------------------*/
__task void Task_ADC_Con(void){
  // timing
	const unsigned int period = 100;
	os_itv_set(period);	
		for(;;){ 
			os_itv_wait();
			/* Do actions below */
			start_ADC();
		}
}	 // End Task_ADC_Con(void)



/* @ Function to convert ADC value to respective step values
 * @ digital value: digital value from ADC convertion (10 bits)
 * @ max_adc_value: maximum ADC value for the analog input
 * @ reference value: Reference value based calculation
*/
int convert_bit_to_value(unsigned short digital_value, int max_adc_value,
													int reference)
{
	/* check 0 condition */	
	if (!digital_value)
		return 0;
	/* divide by zero */
	if (max_adc_value == 0)
		return 0;
	/* let's try not to do floating point calculations 
	 * e.g (300 * 1000) / 600 = 500 (i.e 0.5) 
	 * final calculations (500 * 5) / 1000 = 2.5 (~3)
	 */
	return ((((digital_value * 1000) / max_adc_value) * reference) / 1000);
}

/* @ Function to write to the LCD 
 * @ first_line: Characters for first line of the LCD 
 * @ second_line: Characters for second line of the LCD
*/
void write_to_LCD(unsigned char *first_line, unsigned char *second_line)
{
	os_mut_wait(&LCD_mutex, 0xffff);	/* Enter CS, wait for mutex */

	LCD_gotoxy(1,1);
	LCD_puts(first_line);
	LCD_puts(second_line); 

	os_mut_release(&LCD_mutex);				/* end of CS, release mutex */
}



/*----------------------------------------------------------------------------
 *        Task 'TaskPWM': 
 *
 * PWM implementation
 * Frequency = 100MHZ => Full cycle = 1/100 = 10ms
 * Divide Full Cycle by 5 => Small Cycle = 2ms
 *---------------------------------------------------------------------------*/
__task void TaskPWM(void) {
	int state = PWM_LED_ON;
	const unsigned int taskperiod = 2; // Copy task period value in milliseconds here
	int pwm_counter = 0;
	head_light_on();
	os_itv_set(taskperiod);
	while(1){
		//check special values, do not need to enter the state machine (need 2ms to transit to another state)
		if(low_value == 0)
		{
			if(head_light_status == 0)	head_light_on();
			state = LED_ON;
		}
		else if(high_value == 0)
		{
			if (head_light_status == 1) head_light_off();
			state = LED_OFF;
		}
		
		else
			
			switch(state) { // Transitions
				case -1:
							state = PWM_LED_OFF;				 
				break;
				case PWM_LED_ON:
					
					if(pwm_counter < high_value)
					{
						state = PWM_LED_ON;
						pwm_counter++;
					}	
					else
					{
						pwm_counter = 0;
						state = PWM_LED_OFF;
						head_light_off();
					}	
						
				break;
				case PWM_LED_OFF:
					if(pwm_counter <low_value)
					{
						state = PWM_LED_OFF;
						pwm_counter++;
					}
					else
					{
						pwm_counter = 0;
						state = PWM_LED_ON;
						head_light_on();
					}	
				break;
			}
		
		os_itv_wait();
   } // while (1)
}

/*----------------------------------------------------------------------------
 *        Task  'TaskPWM_InternalLight': PWM for Internal Light
 *
 * PWM implementation
 * Frequency = 100MHZ => Full cycle = 1/100 = 10ms
 * Divide Full Cycle by 5 => Small Cycle = 2ms
*---------------------------------------------------------------------------*/
__task void TaskPWM_InternalLight(void) {
	int state = PWM_LED_ON;
	const unsigned int taskperiod = 2; // Copy task period value in milliseconds here
	int pwm_counter = 0;

	os_itv_set(taskperiod);
	while(1){
			switch(state) { // Transitions
				case -1:
							state = PWM_LED_OFF;				 
				break;
				case PWM_LED_ON:
					
					if(pwm_counter <= high_value_internal_light)
					{
						state = PWM_LED_ON;
						pwm_counter++;
					}	
					else
					{
						pwm_counter = 0;
						state = PWM_LED_OFF;
						internal_light_off();
					}	
						
				break;
				case PWM_LED_OFF:
					if(pwm_counter <= low_value_internal_light)
					{
						state = PWM_LED_OFF;
						pwm_counter++;
					}
					else
					{
						pwm_counter = 0;
						state = PWM_LED_ON;
						internal_light_on();
					}	
				break;
			}
		
		os_itv_wait();
   } // while (1)
}


/*----------------------------------------------------------------------------
 *        Task  'Task_InternalLight_DimmedOff': Internal Light Dimmed Off
 *---------------------------------------------------------------------------*/
__task void Task_InternalLight_DimmedOff(void)
{
	const unsigned int taskperiod = 600; // Copy task period value in milliseconds here
	int count = 0;
	OS_TID internal_light_dimmed_id;	
	os_itv_set(taskperiod);
	high_value_internal_light = 5;
	low_value_internal_light = 0;
	internal_light_on();
	internal_light_dimmed_id = os_tsk_create(TaskPWM_InternalLight,1);
	for(count = 0; count <=5 ; count ++ )
	{
		if(light_on_when_dimming == 1)
			break;
		low_value_internal_light= count;
		high_value_internal_light = 5-count;
		os_itv_wait();
	} 
	
	os_tsk_delete(internal_light_dimmed_id);
	if(light_on_when_dimming == 1)
	{
			light_on_when_dimming = 0;
			internal_light_on();
	}
	
	os_tsk_delete_self ();
}


/*----------------------------------------------------------------------------
 *        Task  'Task_HeadLight_Dimming': Headlight Light Dimmed Off
 *---------------------------------------------------------------------------*/
__task void Task_HeadLight_Dimming(void){
	const unsigned int taskperiod = 400;
	os_itv_set(taskperiod);
	os_tsk_create(TaskPWM,2);
	
	while(1)
	{
		if(engineStatus ==0)
		{		high_value=0;
				low_value =5;
		}
		else
		{
			if(low_value != ambient_light)
			{
					high_value = 5 - ambient_light;
					low_value = ambient_light;
			}
		}
		os_itv_wait();
	}
	
}

/* @ Function to determine the engine state
 * @ state value: previous state
 * @ return current state
 * User can either press the button continously or toggle press the button
*/
int Button_Engine(int state) {
		switch(state) { // Transitions
      case -1:
					state = ENGINE_OFF;				 
      break;
			
			case ENGINE_OFF:
			{
				if (A1 && !engine_release_count) 
				{
          state = ENGINE_ON;
					engineStatus = 1;
					engine_release_count = 0;
        }
				else if (!A1 && engine_release_count) 
				{
					// When engine is at on state, press button to off engine &
					// hold the pressing till now then release
					// so next press is meant to on engine
					engine_release_count = 0;
				}
			}
			break;
				
      case ENGINE_ON:
			{
				if (!A1 && !engine_release_count) 
				{
					// When engine is at off state, press button to on engine &
					// hold the pressing till now then release
					// so next press is meant to off engine
					engine_release_count = 1; 
				}
				else if (A1 && engine_release_count)
				{
					
          state = ENGINE_OFF;
					engineStatus = 0;
				}
			}
			break;
			
      default:
         state = -1;
      } // Transitions

#if 0
   switch(state) { // State actions
		case ENGINE_ON:
		{
			LCD_cls();
			LCD_gotoxy(1,1);
			LCD_puts("ENGINE ON");
			LCD_cur_off ();
		}
		break;
		
		case ENGINE_OFF:
		{
			LCD_cls();
			LCD_gotoxy(1,1);
			LCD_puts("ENGINE OFF");
			LCD_cur_off ();
		}
		break;	
	
		default: // ADD default behaviour below
    break;
   } // State actions
#endif
   
	 //SENSOR_State = state;
	 ENGINE_State = state;
   return state;
}


/* @ Function to determine the door state
 * @ state value: previous state
 * @ return current state
 * User can either press the button continously or toggle press the button
*/
int Button_Door(int state) {
   switch(state) { // Transitions
      case -1:
					state = DOOR_CLOSE;	
      break;
			
			case DOOR_CLOSE:
			{
				if (A0 && !door_release_count) 
				{
          state = DOOR_OPEN;
					doorStatus = 1;
					door_release_count = 0;
        }
				else if (!A0 && door_release_count) 
				{
					// When door is at open state, press button to close door &
					// hold the pressing till now then release
					// so next press is meant to open door
					door_release_count = 0;	
				}
			}
			break;
				
      case DOOR_OPEN:
			{
				if (!A0 && !door_release_count) 
				{
					// When door is at close state, press button to open door &
					// hold the pressing till now then release
					// so next press is meant to close door
					door_release_count = 1; 
				}
				else if (A0 && door_release_count) 
				{
          state = DOOR_CLOSE;
					doorStatus = 0;
				}
			}
			break;			
			
      default:
         state = -1;
      } // Transitions
#if 0
   switch(state) { // State actions
		case DOOR_OPEN:
		{
			//LCD_cls();
			LCD_gotoxy(1,2);
			LCD_puts("Door Open");
			LCD_cur_off ();
		}
		break;
		
		case DOOR_CLOSE:
		{
			//LCD_cls();
			LCD_gotoxy(1,2);
			LCD_puts("Door Close");
			LCD_cur_off ();
		}
		break;	
	
		default: // ADD default behaviour below
    break;
   } // State actions
#endif
   
	 
	 DOOR_State = state;
   return state;
}

/* @ Function to determine the internal light state
 * @ state value: previous state
 * @ return current state
*/
int Internal_Light_State(int state) {
 	
	static int internal_dimming_task_id = -1; 
	
   switch(state) { // Transitions
      case -1:
					state = INT_LIGHT_OFF;				 
      break;	

			case INT_LIGHT_OFF:
			{
				if ((doorStatus && !engineStatus) && (ambient_light < ambient_threshold) && !doorOpen_previously)
				//if ((doorStatus && !engineStatus) && (ambient_light < ambient_threshold))
				{
          state = INT_LIGHT_ON;
					counter = 1;
        }
				else if (!doorStatus) // door closed
				{
					doorOpen_previously = 0; // set to 0 when door closed
				}
			}
			break;
				
      case INT_LIGHT_ON:
			{
				if (engineStatus) // Engine is ON
					state = INT_LIGHT_OFF;
				else if (doorStatus && (ambient_light >= ambient_threshold))
					state = INT_LIGHT_OFF;
				else if (doorStatus && counter < 100) //Wait for (100*200ms) = 20 secs
				{					
					state = INT_LIGHT_ON;
					counter++;
				}
				else if (doorStatus && counter >= 100) //Light ON for >=20 secs
				{					
					state = INT_LIGHT_OFF;
					counter = 0;
					doorOpen_previously = 1; // door remains opened
				}
				else if (!doorStatus) 
				{	
					if (counter < 50) //Wait for (50*200ms) = 10 secs
						counter++;
					else
					{ // Light ON for >= 10 secs
						state = INT_LIGHT_DIM;
						internal_dimming_task_id = os_tsk_create(Task_InternalLight_DimmedOff, 1);
						counter = 1;
					}
				}
			}
			break;
			
			case INT_LIGHT_DIM:
			{
				if (engineStatus) // Engine ON
				{
					state = INT_LIGHT_OFF;
				}
				else if (doorStatus && (ambient_light < ambient_threshold)) 
				{	
          state = INT_LIGHT_ON;
					light_on_when_dimming = 1;
					counter = 1;
				}
				else if (counter >= 20) // Light dim >=4 sec
				//else if (counter >= 50) 
				{				
					state = INT_LIGHT_OFF;
					counter = 0;
				}
				else if (counter < 20) //Wait for (20*200ms) = 4 secs
				//else if (counter < 50) //Wait for (50*200ms) = 10 secs
				{
					state = INT_LIGHT_DIM;
					counter++;
				}
				
				/*if (state != INT_LIGHT_DIM)
				{
					if (internal_dimming_task_id != -1)
					{
						os_tsk_delete(internal_dimming_task_id);
						internal_dimming_task_id = -1;
					}
				}*/
			}
			break;
			
      default:
         state = -1;
    } // Transitions
	 
  	if (state == INT_LIGHT_ON)
			internal_light_on();
		
		if (state == INT_LIGHT_OFF)
			internal_light_off();
	
	  
#if 0
   switch(state) { // State actions
		case INT_LIGHT_ON:
		{
			LCD_cls();
			LCD_gotoxy(1,1);
			LCD_puts("INT LIGHT ON    ");
			LCD_cur_off ();		
		}
		break;
		
		case INT_LIGHT_OFF:
		{
			LCD_cls();
			LCD_gotoxy(1,1);		
			LCD_puts("INT LIGHT OFF   ");
			LCD_cur_off ();
		}
		break;	
		
		case INT_LIGHT_DIM:
		{
			LCD_cls();
			LCD_gotoxy(1,1);			
			LCD_puts("INT LIGHT DIM   ");
			LCD_cur_off ();
		}
		break;			

		default: // ADD default behaviour below
    break;
   } // State actions
 #endif
	 
	 
	 INT_LIGHT_State = state;
   return state;
}

/*----------------------------------------------------------------------------
 *        Task  'Task_Alarm_Blinking': Alarm Blinking
 *---------------------------------------------------------------------------*/
__task void Task_Alarm_Blinking(void){
	const unsigned int taskperiod = 300; // Copy task period value in milliseconds here
	int alarm_state = 0;
	os_itv_set(taskperiod);	
	while(1)
	{
		if (alarm_state==0)
		{
			alarm_state = 1;
			alarm_on();
		}
		else
		{
			alarm_state = 0;
			alarm_off();
		}
		os_itv_wait();
	}
}

/* @ Function to determine the alarm state
 * @ state value: previous state
 * @ return current state
*/
int Alarm_State(int state) {
	static int alarm_task_id = -1; 

   switch(state) { // Transitions
      case -1:
					state = ALARM_OFF;				 
      break;
			
			case ALARM_OFF:
			{
				if (doorStatus && engineStatus) 
				{		
					state = ALARM_ON;
					
					//alarm_on();
					alarm_task_id = os_tsk_create(Task_Alarm_Blinking,3);
        }
			}
			break;
				
      case ALARM_ON:
			{
				if (!doorStatus || !engineStatus) 
				{						
					state = ALARM_OFF;

					if (alarm_task_id!= -1)
						os_tsk_delete(alarm_task_id);
				
					alarm_off();
				}
			}
			break;
			
      default:
         state = -1;
      } // Transitions

#if 0
   switch(state) { // State actions
		case ALARM_ON:
		{
			//LCD_cls();
//			LCD_gotoxy(1,2);
//			LCD_puts("ALARM ON        ");
//			LCD_cur_off ();
		}
		break;
		
		case ALARM_OFF:
		{
			//LCD_cls();
			LCD_gotoxy(1,2);
			LCD_puts("ALARM OFF      ");
			LCD_cur_off ();
		}
		break;	
	
		default: // ADD default behaviour below
    break;
   } // State actions
 #endif
	 
	 
	 ALARM_State = state;
   return state;
}

/*----------------------------------------------------------------------------
 *        Task  'Task_Door_Engine': Door, Engine, Internal Light, Alarm
 *---------------------------------------------------------------------------*/
__task void Task_Door_Engine(void) {
	int int_light_state = -1, alarm_state = -1, door_state = -1, engine_state = -1;

  const unsigned int taskperiod = 200; // Copy task period value in milliseconds here
  os_itv_set(taskperiod);
	
	while(1){
		read_buttons();
		engine_state = Button_Engine(engine_state);
		door_state = Button_Door(door_state);
		
		int_light_state = Internal_Light_State(int_light_state);
		alarm_state = Alarm_State(alarm_state);
		os_itv_wait();
	}
	
}

int get_acceleration(int prev_speed, int cur_speed)
{
	int magnitude = 0;
	
	magnitude = abs(cur_speed - prev_speed) / 5;
	if (magnitude)
		return magnitude;
	else
		return 2;
}

/*----------------------------------------------------------------------------
 *        Task  'Task_Sensor': 
 *---------------------------------------------------------------------------*/
__task void Task_Sensor(void) {
	//int state = -1;
	int cur_speed, cur_ambient, speed = 0; // commented off on 21 Mar
	int collisionSpeed = 0;
	int prev_speed = 0;
	int count = 0;
	int gpio_data;
	int collisionDistance;
	int acceleration, past_speed = 0;
	unsigned char LCD_first_line[16], LCD_second_line[16];
	//const unsigned int taskperiod = 200; // Copy task period value in milliseconds here
	//os_itv_set(taskperiod);
	struct adc_mbox *mbox;
	
	collisionAvoidance = 0; //initialize
	
	memset(LCD_first_line, 0, sizeof(LCD_first_line));
	memset(LCD_second_line, 0, sizeof(LCD_second_line));
	
	while(1){
		os_mbx_wait(mailbox1, (void **) &mbox, 0xffff); 	/* wait for mailbox send */
		
		/* read GIPO 9.3 value */
		gpio_data = (GPIO9->DR[0x20] >> 3);
		
		/* convert bit value to speed. Max reading of ADC = 552
     * And we set the maximum speed to 200KM/H
		 */
		if (engineStatus == 0)	{// if engine not on, speed should be zero
			cur_speed = 0;
			speed = 0;
		}
		else {
			cur_speed = convert_bit_to_value(mbox->SliderSensor, 552, 200); 
			if (cur_speed >= MAX_SPEED)
				cur_speed = MAX_SPEED;
		}

		/* stabalize current speed values */
		if (!((cur_speed - prev_speed >= 2) || (prev_speed - cur_speed >= 2)))
			cur_speed = prev_speed;
		else
			prev_speed = cur_speed;
		
		/* IR Sensor when no obstruction (between 0x227 - 0x22B) 
		 * IR Sensor when there is an object at 5mm & below (0x210 - 0x170) 
		 * collisionAvoidance flag set when it detects an obstruction  
		 */
		if (mbox->IRSensor < 0x220) {
			collisionAvoidance = 1;
			collisionSpeed = cur_speed;
			/* object very close to the car */
			if (mbox->IRSensor < 0x1A0)
				collisionDistance = 2;
			else
				collisionDistance = 1;
		}
		else {
			collisionAvoidance = 0;
			collisionDistance = 0;
		}
		
		/* We set the potentiometer max ADC readings as 950, maximum brightness = 5 */
		cur_ambient = (convert_bit_to_value(mbox->Potentiometer, 950, 5));
		if (cur_ambient > 5)
			cur_ambient = 5;
		
		ambient_light = cur_ambient;
		
		/* If collisionAvoidance flag is set and current distance is below treshold 
		* 	then: we slowly decrease the speed to zero (slow down and stop vehicle).
		*	 else if brake is pressed and speed is more then zero
		*		then: we slowly decrease the speed. If speed is zero or negative, set it to zero
		*	 else
		*		we set the current speed to the speedometer speed
		*/
		if (collisionAvoidance == 1 && (collisionDistance > 1)) {
			speed -= 1;
			if (speed <= 0) 
				speed = 0;
		} else if (gpio_data == 1 && speed >= 0) {
			speed -= 5;
			if (speed <= 0)
				speed = 0;
		}
		else {
			/* To slow down the speed up and down on the LCD */
			count++;
			if (count == 5) {
				count = 0;
				
				if (collisionSpeed > 0) {
					collisionSpeed = 0;
					past_speed = collisionSpeed;
				}
				
				acceleration = get_acceleration(past_speed, cur_speed);
				if (speed < cur_speed)
					((speed + acceleration) > cur_speed) ? speed = cur_speed : (speed += acceleration);
				else if (speed > cur_speed)
					(speed - acceleration < cur_speed) ? speed = cur_speed : (speed -= acceleration);
				else 
					speed = cur_speed;
			} 
			
			if (count >= 20) {
				count = 0;
				past_speed = cur_speed;
			}
		}
		/* if we detect collision, print warning  */
		if (collisionAvoidance == 0 || engineStatus == 0) {
			sprintf((char *) LCD_first_line, "SPEED:   %3dKM/H", speed);
			sprintf((char *) LCD_second_line, "LIGHT:         %d", cur_ambient);
		} else {
			sprintf((char *) LCD_first_line, "%s", FIRST_LINE);
			sprintf((char *) LCD_second_line, "SPEED:   %3dKM/H", speed);
		}
		
		/* to test the two functions */
		write_to_LCD(LCD_first_line, LCD_second_line);


		_free_box(mpool, mbox);			/* free allocated mem */
		//os_itv_wait();
	} // while (1)
}

/*----------------------------------------------------------------------------
 *        Task  'TASK_TEST_LED': Test LED
 *---------------------------------------------------------------------------*/
__task void TASK_TEST_LED(void){
	head_light_on();	
	os_dly_wait(400);
	head_light_off();
	os_dly_wait(400);
	internal_light_on();
	os_dly_wait(400);
	internal_light_off();
	os_dly_wait(400);
	alarm_on();
	os_dly_wait(400);
	alarm_off();
	os_tsk_delete_self ();
}

/*----------------------------------------------------------------------------
 *        Task 0 'init': Initialize
 *---------------------------------------------------------------------------*/
__task void init (void) {

	unsigned int n = 0;
		  
	/* Set up Potentiometer and Light sensor*/                                                              

	SCU->GPIOIN[4]  |= 0x07;                /* P4.0, P4.1 and P4.2 input - mode 0 */
	SCU->GPIOOUT[4] &= 0xFFC0;              /* P4.0 output - mode 0             */

	/* To look up search for GPIO4_DIR in the peripherals manual 				  			*/
	GPIO4->DDR      &= 0xF8;                /* P4.0 and P4.1 direction - input  */
	SCU->GPIOANA    |= 0x0007;              /* P4.0 analog mode ON              */
		
	SCU->GPIOIN[9]  |= 0x0f;                /* P4.0, P4.1 and P4.2 input - mode 0 */
	//SCU->GPIOOUT[7] &= 0xFFC0;              /* P4.0 output - mode 0             */
	GPIO9->DDR      &= 0xf0;                /* P4.0 and P4.1 direction - input  */

	/* To understand the ADC configuration register consult the manual 				  */
	ADC->CR         |= 0x0042;              /* Set POR bit                      */
	for (n = 0; n < 100000; n ++);          /* Wait > 1 ms  (at 96 MHz)         */

	ADC->CR         &= 0xFFB7;              /* Clear STB bit                    */
	for (n = 0; n < 1500; n ++);            /* Wait > 15 us (at 96 MHz)         */

	ADC->CR         |= 0x0423;              /* Enable end of conversion interupt*/
	ADC->CCR         = 0x003F;              /* AD Conversion for Channels 0,1,2, No WDG on Ch 0    */

	/* Configure and enable IRQ for A/D Converter (ADC)                         */
	VIC0->VAiR[15]  = (unsigned int)ADC_IRQ_Handler; /* Setup ADC IRQ Hndl addr */
	VIC0->VCiR[15] |= 0x20;                 /* Enable the vector interrupt      */
	VIC0->VCiR[15] |= 15;                   /* Specify the interrupt number     */
	VIC0->INTER    |= (1<<15);              /* Enable ADC interrupt             */

	/* Configuring LED                     */
	SCU->GPIOOUT[7]  = 0x5555;
	GPIO7->DDR       = 0xFF;
	GPIO7->DR[0x3FC] = 0x00;

	/* LCD Setup                           */
	GPIO8->DDR       = 0xFF;
	GPIO9->DDR       = 0x07;

	/* Port 3 setup for button 3.5 and 3.6 */
	SCU->GPIOIN[3]  |= 0x60;
	SCU->GPIOOUT[3] &= 0xC3FF;
	GPIO3->DDR      &= 0x9F;

	LCD_init(); //Initialize LCD
	LCD_cur_off(); //Remove LCD cursor
	LCD_cls(); //Clearing LCD screen

	os_mbx_init(&mailbox1, sizeof(mailbox1));		/* Initialize mailbox */
	/* Initialize LCD & LED mutexes */
	os_mut_init(&LCD_mutex);
	os_mut_init(&LED_mutex);
	
	LCD_on(); // Turn on LCD
	LCD_cls();
	
	//Launch the task in the following manner
	counter=0;
	ADC_task_id = os_tsk_create(Task_ADC_Con,5);
	IO_output_id = os_tsk_create(Task_Sensor,5);
	MAIN_state_machine = os_tsk_create(Task_Door_Engine,4);
	
	os_tsk_create(Task_HeadLight_Dimming,1);
//os_tsk_create(TASK_TEST_LED,0);

	os_tsk_delete_self ();
}


/*----------------------------------------------------------------------------
 *        Main: Initialize and start RTX Kernel
 *---------------------------------------------------------------------------*/
int main (void) {
  _init_box(mpool, sizeof(mpool), sizeof(struct adc_mbox));	/* Initialize memory pool */
	os_sys_init (init);                    /* Initialize RTX and start init    */
	return 0;
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
